# CSCI 432 Project

## Overview

In this project, groups of two to five  students will collaborate to investigate an
algorithm from a "recent" conference or journal. Each
group will choose the topic on their own, and become experts on this algorithm.
In addition, they will take the paper that they found one step further
in the plus-one (+1) element of their project.

## Deadlines
All deadlines are by 23:59 Mountain time on the date given (except for the
in-class presentations and discussion). Although not mandatory, all teams are
encouraged
to meet with the instructor or the TA between the P-1
and P-3 deadlines.

* P-1: due 11 October.  Write a one-page summary about an algorithm that you find 
  particularly interesting.  Briefly describe the problem and algorithm, and
  explain why you find it interesting.  
* Project groups will be assigned by 15 October. 5 points.
* P-2: due 30 October. Investigate three different algorithms. For each algorithm, you 
  must identify the problem being solved.  Do not go into the details of the
  algorithms itself, unless they solve the same problem.  The deliverable is a 
  two-to-three page write-up (submitted as a PDF).  10 points.
* P-3: due 11 November. Selection of algorithm: You must select the algorithm that you 
  will be investigating. This does not need to be one of the algorithms discussed in 
  P-2, but is likely to be one of those. The deliverable is an (up to) two-page write-up
  identifying the problem being solved and providing pseudo-code for the algorithm itself. 
  Not all steps need to be fully explained, but the general essence of the
  algorithm should be clear. 10 points.
* P-4: due 18 November.  Identification of what the +1 element of your project will be: 
  Suggestions of ways to do a +1: improve the algorithm in some way (speed, clarity), 
  explain the analysis of the algorithm in a different way than provided in the paper, 
  provide a runtime analysis if there is none, compare it against two or three other 
  algorithms, walk through an example or two, explain the algorithm more clearly/concisely 
  than the original publication, thoroughly explaining one of their proofs, etc. You are
  free to be as creative as you want for your +1 element, but I highly suggest
  you discuss this with the instructor or TA before this deadline. Your +1 is going
  to be the focus of your video, so choose wisely.  The deliverable is an (up to) three-page 
  write-up identifying the problem, providing pseudo-code for the algorithm, 
  and identifying your +1 element. 10 points.
* P-5: due 25 November. Demonstration of progress: Prepare a document outlining (a) what
  you have done so far and (b) what is left to be done.  The deliverable is a one-to-two 
  page write-up (submitted as a PDF). 5 points.
* P-6: last week of class.  Video presentations: Each group will 
  present their video in-class. You will have to introduce the video, and answer
  questions from fellow students. 
  In addition, each group member will turn in a one-page summary detailing their contributions,
  the strengths / weaknesses of their project, and explain one thing that would be
  done differently if you could do it again. 40 points for video, 5 points for
  personal reflection.
* P-7: Final Exam Time.  Peer evaluation: We will discuss the
  peer-evaluations that were filled out during the video presentations. 
  During the final meeting period, we will form a mock program committee (to be assigned / 
  formed later in the semester) will summarize their individual evaluations.  BEFORE the final 
  meeting period, peer evaluations must be shared with the group. You will not be graded on 
  how well your peers evaluate your project, but rather, how well you evaluate other projects.  
  The devlierable is a group report (to be written and submitted in-class). 10 points.
* EC: Up to five points of extra credit can be earned for using version control of LaTex 
  documents and other (non-video) project files. The deliverables should be either .txt,  
  .tex, or code files maintained on a mercurial or git repository hosted on bitbucket or 
  github. Microsoft Word documents do not play nicely with version control, so no credit will 
  be given if Word files are in the repo. 5 points.

## Project Suggestions
List of possible papers from which to base a project:

* Ahmed, Fasy, Wenk. Path-Based Distance for Street Map Comparison. ACM TSAS, 2015.
* Alon, Babai, and Itai. A Fast and Simple Randomized Parallel Algorithm for the Maximal Independent Set Problem/. Journal of Algorithms, 1986.
* Aloupis, Fevens, Langerman, Matsui, Mesa, Nu˜nez, Rappaport, and Toussaint. Algorithms for Computing Geometric Measures of Melodic Similarity. Computer Music Journal, 2006.
* Bethea and Reiter. Data Structures with Unpredictable Timing. ESORICS, 2009.
* Chambers, de Verdire, Erickson, Lazard, Lazarus, and Thite. Homotopic Frchet Distance between Curves or, Walking your Dog in the Woods in Polynomial Time. Computational Geometry, 2010.
* Chan. Optimal output-sensitive convex hull algorithms in two and three dimensions. Discrete and Computational Geometry, 1996.
* Chen and Kerber. Persistent Homology Computation with a Twist. EuroCG, 2011.
* Chazal, Fasy, Lecci, Rinaldo, Singh, and Wasserman. On the Bootstrap for Persistence Diagrams and Landscapes. Modeling and Analysis of Information Systems, 2013.
* Edelsbrunner and Guibas. Topologically Sweeping an Arrangement. Journal of Computer and System Sciences, 1989.
* Edelsbrunner and Pausinger. Approximation and Convergence of the Intrinsic Volume. Adv. Math. 287 (2016), 674-703
* Fajstrup, Goubault,  Haucourt,  Mimram, and Raussen.  Trace spaces: An Efficient New Technique for State-space Reduction. ESOP, 2012.
* Guibas, and Oudot. Reconstruction Using Witness Complexes. DCG, 2008.
* Li and Svensson. Approximating k-Median via Pseudo-Approximation. STOC, 2013.
* Liota. Low Degree Algorithms for Computing and Checking Gabriel Graphs, Brown University Technical Report, 1996.
* Miller and Sheehy. Approximate Centerpoints with Proofs. CGTA, 2010.
* Millman, Love, Chan, and Snoeyink. Computing the Nearest Neighbor Transform Exactly with only Double Precision. ISVD, 2012.
* Milosavljevic, Morozov, and Skraba. Zigzag Persistent Homology in Matrix Multiplication Time. SoCG, 2011.
* Pratt, Riley, and Sheehy. Exploring Circle Packing Algorithms. SoCG, 2016.
* Shonkwiler. An Image Algorithm for Computing the Hausdorff Distance Efficiently in Linear Time. Information Processing Letters, 1989.
* Shor. Polynomial-Time Algorithms for Prime Factorization and Discrete Logarithms on a Quantum Computer. SIAM Journal on Computing, 1997.
* Oostrum and Veltkamp. Parametric Search Made Practical. SoCG 2002.
* Wang, Wang, and Li. Efficient Map Reconstruction and Augmentation via     Topological Methods. ACM SIGSPATIAL GIS, 2015.
* Zheng, Jestes, Phillips, Li. Quality and Efficiency in Kernel Density Estimates for Large Data. ACM Conference on the Management of Data (SIGMOD), 2013.

The list of papers above is only a sprinkling of recent developments in
algorithms. I encourage teams to peruse the following (non-exhaustive) list
of venue for finding other papers:

* ESA: European Symposium on Algorithms.
* EuroCG: European Workshop on Computational Geometry.
* GD: International Symposium on Graph Drawing.
* ISAAC: International Symposiums on Algorithms and Computation.
* LATIN: Latin American Theoretical INformatics.
* SIGSPATIAL GIS
* SoCG; Symposium on Computational Geometry.
* SODA: Symposium on Discrete Algorithms.
* STOC: ACM Symposium on Theory of Computing.
* SWAT: Scandinavian Workshop on Algorithm Theory.
* WADS: Algorithms and Data Structures Symposium.

Also check out algorithm competitions, such as the Geometric Optimization
Challenge.
